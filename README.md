# README #

Hello here at my homepage! :) 

To start the application you have to:

1. Get all the files in one folder
2. Use virtualenv to isolate this app (virtualenv %folder% --python=python2.7)
3. cd to the directory
4. source bin/activate
5. pip install -r requirements.txt
6. python web_lb_1.py &
7. get your ip from #ifconfig and go to %your_ip%:5000 via web browser
