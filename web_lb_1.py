from flask import Flask, request,render_template
import sqlite3
import datetime
import time
import subprocess
import os
import os.path
import random

app = Flask(__name__)

@app.route("/")
def root():
	return render_template('home.html')

@app.route("/home")
def home():
	return render_template('home.html')


@app.route("/school")
def school():
	return render_template('school.html')

@app.route("/computer")
def computer():
	return render_template('computer.html')

@app.route("/hobbies")
def hobbies():
	return render_template('hobbies.html')

@app.route("/ai")
def ai():
	return render_template('ai.html')

@app.route("/plans")
def plans():
	return render_template('plans.html')


if __name__ == "__main__":
	app.run(host='0.0.0.0',port=5000,debug=True)
	
